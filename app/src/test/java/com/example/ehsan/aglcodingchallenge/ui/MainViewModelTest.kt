package com.example.ehsan.aglcodingchallenge.ui

import com.example.ehsan.aglcodingchallenge.data.people.PeopleRepository
import com.example.ehsan.aglcodingchallenge.model.Person
import com.example.ehsan.aglcodingchallenge.model.Pet
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class MainViewModelTest {

    companion object {
        const val CAT = "Cat"
        const val MALE = "Male"
        const val Female = "Female"
    }

    private lateinit var mainViewModel: MainViewModel

    private lateinit var mockRepository: PeopleRepository

    private lateinit var person1: Person
    private lateinit var person2: Person
    private lateinit var person3: Person
    private lateinit var person4: Person
    private lateinit var person5: Person
    private lateinit var person6: Person
    private lateinit var person7: Person


    private lateinit var pet1: Pet
    private lateinit var pet2: Pet
    private lateinit var pet3: Pet
    private lateinit var pet4: Pet
    private lateinit var pet5: Pet
    private lateinit var pet6: Pet

    @Before
    fun setUp() {
        pet1 = Pet("pname1", CAT)
        pet2 = Pet(null, CAT)
        pet3 = Pet(null, "Dog")
        pet4 = Pet("pname4", "Dog")
        pet5 = Pet("pname5", null)
        pet6 = Pet(null, null)

        person1 = Person(1, "", "name1", null)
        person2 = Person(1, MALE, "name2", listOf(pet1, pet2, pet3, null, pet4))
        person3 = Person(1, Female, "name2", listOf(pet1, pet1))
        person4 = Person(1, Female, "name2", listOf(pet6, pet5, pet4, pet3))
        person5 = Person(1, "1", "name2", listOf(null, null))
        person6 = Person(1, null, "name2", listOf(pet5, null))
        person7 = Person(1, null, "name2", listOf(pet1, pet2, pet1, pet2))

        mockRepository = mock()
        mainViewModel = MainViewModel(mockRepository)
    }

    @Test
    fun `personToCatSortedList null pet list`() {
        val catList = mainViewModel.personToCatSortedList(listOf(person1))

        assertEquals(0, catList.size)
    }

    @Test
    fun `personToCatSortedList 1`() {
        val catList = mainViewModel.personToCatSortedList(listOf(person1, person2))

        assertEquals(2, catList.size)
        assertTrue(catList.contains(Cat("")))
    }

    @Test
    fun `personToCatSortedList 2`() {
        val catList = mainViewModel.personToCatSortedList(listOf(person4, person5, person6))

        assertEquals(0, catList.size)
    }

    @Test
    fun `personToCatSortedList 3`() {
        val catList = mainViewModel.personToCatSortedList(listOf(person7))

        assertEquals(4, catList.size)
    }
}