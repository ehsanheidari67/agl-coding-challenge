package com.example.ehsan.aglcodingchallenge.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.aglcodingchallenge.R
import kotlinx.android.synthetic.main.recycler_item_main.view.*

class MainAdapter(private var items: List<Cat>) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_main, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun setData(items: List<Cat>) {
        this.items = items
        notifyDataSetChanged()
    }

    class MainViewHolder(val catItemView: View) : RecyclerView.ViewHolder(catItemView) {
        fun bind(cat: Cat) {
            catItemView.cat_name_text_view.text = cat.name
        }
    }

}