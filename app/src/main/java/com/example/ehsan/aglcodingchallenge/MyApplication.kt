package com.example.ehsan.aglcodingchallenge

import android.app.Application
import com.example.ehsan.aglcodingchallenge.di.ApplicationComponent
import com.example.ehsan.aglcodingchallenge.di.ApplicationModule
import com.example.ehsan.aglcodingchallenge.di.DaggerApplicationComponent
import timber.log.Timber

class MyApplication : Application() {

    val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        plantTimber()
        injectMembers()
    }

    private fun injectMembers() {
        appComponent.inject(this)
    }

    private fun plantTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}