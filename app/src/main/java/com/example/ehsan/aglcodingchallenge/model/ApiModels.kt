package com.example.ehsan.aglcodingchallenge.model

data class Person(
    val age: Int?,
    val gender: String?,
    val name: String?,
    val pets: List<Pet?>?
)

data class Pet(
    val name: String?,
    val type: String?
)