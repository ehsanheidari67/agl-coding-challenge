package com.example.ehsan.aglcodingchallenge.data.people

import javax.inject.Inject

class PeopleDataSourceImpl @Inject constructor(private val peopleService: PeopleService) : PeopleDataSource {
    override fun people() = peopleService.people().map {
        it.body()
    }
}