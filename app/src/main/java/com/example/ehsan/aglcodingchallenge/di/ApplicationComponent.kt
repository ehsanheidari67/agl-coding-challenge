package com.example.ehsan.aglcodingchallenge.di

import android.app.Application
import com.example.ehsan.aglcodingchallenge.di.viewmodel.ViewModelModule
import com.example.ehsan.aglcodingchallenge.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(application: Application)
    fun inject(mainActivity: MainActivity)
}