package com.example.ehsan.aglcodingchallenge.data.people

import com.example.ehsan.aglcodingchallenge.model.Person
import io.reactivex.Single

interface PeopleDataSource {
    fun people(): Single<List<Person>?>
}