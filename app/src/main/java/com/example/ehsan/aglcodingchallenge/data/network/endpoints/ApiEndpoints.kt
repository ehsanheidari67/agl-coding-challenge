package com.example.ehsan.aglcodingchallenge.data.network.endpoints

import com.example.ehsan.aglcodingchallenge.model.Person
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface PeopleApi {
    @GET("/people.json")
    fun people(): Single<Response<List<Person>>>
}