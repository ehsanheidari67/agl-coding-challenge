package com.example.ehsan.aglcodingchallenge.data.people

import com.example.ehsan.aglcodingchallenge.data.network.endpoints.PeopleApi
import com.example.ehsan.aglcodingchallenge.model.Person
import io.reactivex.Single
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class PeopleService @Inject constructor(retrofit: Retrofit) : PeopleApi {

    private val peopleApi: PeopleApi by lazy {
        retrofit.create(PeopleApi::class.java)
    }

    override fun people(): Single<Response<List<Person>>> = peopleApi.people()
}