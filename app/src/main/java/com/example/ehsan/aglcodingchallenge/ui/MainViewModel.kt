package com.example.ehsan.aglcodingchallenge.ui

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModel
import com.example.ehsan.aglcodingchallenge.Constants
import com.example.ehsan.aglcodingchallenge.data.people.PeopleRepository
import com.example.ehsan.aglcodingchallenge.model.Person
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(private val peopleRepository: PeopleRepository) : ViewModel() {
    val maleOwnerCatListPublishSubject: PublishSubject<List<Cat>> = PublishSubject.create()
    val femaleOwnerCatListPublishSubject: PublishSubject<List<Cat>> = PublishSubject.create()

    private var disposable: Disposable? = null

    fun getPeople() {
        disposable = peopleRepository.people()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { personList ->
                personList.groupBy { person ->
                    person.gender
                }.forEach { sexMap ->
                    when (sexMap.key) {
                        Constants.MALE -> maleOwnerCatListPublishSubject.onNext(personToCatSortedList(sexMap.value))
                        Constants.FEMALE -> femaleOwnerCatListPublishSubject.onNext(personToCatSortedList(sexMap.value))
                    }
                }
            }.subscribe({ }, {
                Timber.e(it)
            })
    }

    @VisibleForTesting
    fun personToCatSortedList(personList: List<Person>): List<Cat> {
        return personList.flatMap {
            it.pets?.filterNotNull() ?: listOf()
        }.filter {
            it.type == Constants.CAT
        }.map {
            Cat(it.name ?: "")
        }.sortedBy {
            it.name
        }
    }

    fun onDestroy() {
        disposable?.dispose()
    }
}