package com.example.ehsan.aglcodingchallenge.ui

import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ehsan.aglcodingchallenge.MyApplication
import com.example.ehsan.aglcodingchallenge.R
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var maleListDisposable: Disposable? = null
    private var femaleListDisposable: Disposable? = null

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as MyApplication).appComponent.inject(this)

        val maleAdapter = MainAdapter(listOf())
        male_recycler_view.layoutManager = LinearLayoutManager(this)
        male_recycler_view.adapter = maleAdapter

        val femaleAdapter = MainAdapter(listOf())
        female_recycler_view.layoutManager = LinearLayoutManager(this)
        female_recycler_view.adapter = femaleAdapter

        mainViewModel.getPeople()

        maleListDisposable = mainViewModel.maleOwnerCatListPublishSubject.subscribe {
            Timber.i(it.toString())
            maleAdapter.setData(it)
        }

        femaleListDisposable = mainViewModel.femaleOwnerCatListPublishSubject.subscribe {
            Timber.i(it.toString())
            femaleAdapter.setData(it)
        }
    }

    override fun onDestroy() {
        dispose()
        super.onDestroy()
    }

    @VisibleForTesting
    fun dispose() {
        mainViewModel.onDestroy()
        maleListDisposable?.dispose()
        femaleListDisposable?.dispose()
    }
}