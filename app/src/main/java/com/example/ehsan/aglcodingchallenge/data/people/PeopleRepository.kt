package com.example.ehsan.aglcodingchallenge.data.people

import com.example.ehsan.aglcodingchallenge.model.Person
import io.reactivex.Single
import javax.inject.Inject

class PeopleRepository @Inject constructor(private val peopleDataSource: PeopleDataSource) : PeopleDataSource {
    override fun people(): Single<List<Person>?> = peopleDataSource.people()
}